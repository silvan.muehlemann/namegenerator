
AI namegenerator
================

Demonstrates how artificial intelligence can come up with fantasy names for cities, girls and startups.

# Generate the models

A list of names is provided in `data/` as csv files. The Python Notebook `Name generation.ipynb`
uses those csv files and trains a LTSM type neural network. The resulting model is then saved in `models/` as 
`.h5` file.

The Flask app in `app.py` uses those models and serves a REST entpoint which provides random names. Those
names are presented by a Vue.js frontend at `frontend/namegenerator`

## Develop

Run the backend:

    flask run
    
Run the frontend in development mode: 
 
    cd frontend/namegenerator
    yarn serve
    
## Build

    cd frontend/namegenerator
    yarn build
    

    