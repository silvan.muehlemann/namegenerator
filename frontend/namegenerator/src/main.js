import Vue from 'vue';
import App from './App.vue';
import VueSlideBar from 'vue-slide-bar';

// Vue.config.productionTip = false;
Vue.component('vue-slide-bar', VueSlideBar);

new Vue({
  render: h => h(App),
}).$mount('#app');
