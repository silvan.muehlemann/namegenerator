from flask import Flask, jsonify, request, send_from_directory, send_file, make_response
import tensorflow as tf

from cantondetection import Cantondetection
from namegeneration import *

schema_classes = [
    # Babynames,
    Swissnames,
    Startups,
    Ortsnamen
]
generators = {}


def create_app():
    app = Flask(__name__)

    def run_on_start(*args, **argv):
        """
        Load trained models at startup
        """
        print("Loading models")
        for cls in schema_classes:
            schema = cls()
            name = cls.__name__
            print(f"\n--Loading Schema {name}")
            schema.load()
            generators[name] = NameGenerator(schema)
            if not generators[name].load_model():
                print("No model found. Train them in Jupyter lab")

        app.cantondetection = Cantondetection()

    run_on_start()
    return app


app = create_app()


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('frontend/namegenerator/dist/js', path)


@app.route('/img/<path:path>')
def send_img(path):
    return send_from_directory('frontend/namegenerator/dist/img', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('frontend/namegenerator/dist/css', path)

@app.route('/static/<path:path>')
def send_static(path):
    return send_from_directory('static', path)

@app.route('/')
def home():
    return send_file('frontend/namegenerator/dist/index.html')


@app.route('/predict/<schema>')
def getname(schema):
    temperature = float(request.args.get('temperature', 1.2))
    name = generators[schema].generate_names(50, temperature=temperature)[0]

    if schema=='Ortsnamen' and random.random() > 0.8:
        # Guess the canton in 0% of the cases
        name = f"{name}<br>({random.choice(app.cantondetection.predict(name,3))})"

    resp = make_response(jsonify(name))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp


if __name__ == '__main__':
    app.run(host='0.0.0.0')
