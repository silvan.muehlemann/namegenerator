import keras
from keras import layers
import pandas
import h5py
import os
import tensorflow as tf
import numpy as np


class Cantondetection:
    """ """

    def __init__(self):
        df = pandas.read_csv(
            "data/ortsnamen.csv", sep=';')

        self.graph = tf.get_default_graph()

        # Length of extracted character sequences
        self.maxlen = 30

        # This holds our extracted sequences
        sentences = []

        # This holds the targets (the follow-up characters) - now canton
        canton_targets = []

        text = ''

        for i in range(0, len(df)):
            town = str(df['ORTNAME'][i]).ljust(30)
            text += town
            canton = df['KTKZ'][i]

            if type(canton) == str:
                canton_targets.append(canton)
                sentences.append(town)

        print('Number of sequences:', len(sentences))

        # List of unique characters in the corpus
        self.chars = sorted(list(set(text)))
        print('Unique characters:', len(self.chars))
        # Dictionary mapping unique characters to their index in `self.chars`
        self.char_indices = dict((char, self.chars.index(char)) for char in self.chars)

        # Next, one-hot encode the characters into binary arrays.
        print('Vectorization...')
        x = np.zeros((len(sentences), self.maxlen, len(self.chars)), dtype=np.bool)
        # y = np.zeros((len(sentences), len(self.chars)), dtype=np.bool)
        for i, sentence in enumerate(sentences):
            for t, char in enumerate(sentence):
                x[i, t, self.char_indices[char]] = 1

        # One-hot encode self.cantons
        self.cantons = sorted(list(set(canton_targets)))
        print('Unique self.cantons:', len(self.cantons))
        # Dictionary mapping unique characters to their index in `self.chars`
        canton_indexes = dict((canton, self.cantons.index(canton)) for canton in self.cantons)

        # Next, one-hot encode the characters into binary arrays.
        print('Vectorization...')
        x = np.zeros((len(sentences), self.maxlen, len(self.chars)), dtype=np.bool)
        # y = np.zeros((len(sentences), len(self.chars)), dtype=np.bool)
        for i, sentence in enumerate(sentences):
            for t, char in enumerate(sentence):
                x[i, t, self.char_indices[char]] = 1

        y = np.zeros((len(canton_targets), len(canton_indexes)), dtype=np.bool)
        canton_indexes = dict((canton, self.cantons.index(canton)) for canton in self.cantons)
        for i, canton_target in enumerate(canton_targets):
            y[i, canton_indexes[canton_targets[i]]] = 1

        self.model = keras.models.Sequential()
        self.model.add(layers.LSTM(196, input_shape=(self.maxlen, len(self.chars))))
        self.model.add(layers.Dense(len(canton_indexes), activation='softmax'))

        from keras.models import load_model

        self.graph = tf.get_default_graph()
        filepath = os.path.join('models', f'city-to-canton.h5')
        self.model = load_model(filepath)

    def vectorize_town(self, input):
        padded = input.ljust(30)

        x = np.zeros((1, self.maxlen, len(self.chars)), dtype=np.bool)
        for t, char in enumerate(padded):
            x[0, t, self.char_indices[char]] = 1

        return x
    
    def predict(self, townname, num_results):
        with self.graph.as_default():
            prediction = self.model.predict(self.vectorize_town(townname))
            return np.array(self.cantons)[prediction[0].argsort()[-num_results:]]
