FROM python:3.6

WORKDIR /code

ADD . /code

RUN pip install -r requirements.txt

EXPOSE 8000

CMD gunicorn -b 0.0.0.0:8000 app:app