import keras
import pandas
import h5py
import os
import tensorflow as tf

import numpy as np
import random
import sys


class Schema:
    # Contains list of all names in the training data
    all_names = []

    fix_case = True
    epochs = 20
    neurons = 128
    modelname = None

    # Whether to uppercase the first letter of the name
    ucfirst = False

class Babynames(Schema):
    modelname = 'babynames'

    def load(self):
        df = pandas.read_csv(
            "data/baby-names.csv",
            sep=',')
        is_year = df['year'] < 1930
        is_probability = df['percent'] > 0.004

        self.all_names = [name.lower() for name in list(df[is_year & is_probability]['name'])]


class Swissnames(Schema):
    modelname = 'swissnames'

    def load(self):
        df = pandas.read_csv(
            "data/swiss-names.csv",
            sep=';',
            usecols=[1, 2, 3],
            names=['name', 'female', 'male'],
            thousands='’',
            skiprows=5)
        df['female'] = df['female'].apply(lambda x: 0 if x == '*' else x)
        df['female'] = df['female'].apply(lambda x: int(x.replace('’', '')) if type(x) is str else x)
        gender_filter = df['female'] > 500
        self.all_names = [name.lower() for name in list(df[gender_filter]['name'])]


class Ortsnamen(Schema):
    fix_case = False
    modelname = 'ortsnamen'
    ucfirst = True

    def load(self):
        df = pandas.read_csv(
            "data/ortsnamen.csv", sep=';')
        self.all_names = [str(name).replace(" ", "_") for name in list(df['ORTNAME'])]


class Startups(Schema):
    fix_case = False
    epochs = 50
    modelname = 'startups-old'
    neurons = 196

    def load(self):
        df = pandas.read_csv(
            "data/startups.csv")
        self.all_names = [str(name).replace(" ", "_") for name in df['name'][0:10000]]


class NameGenerator:
    # Length of extracted character sequences
    maxlen = 60

    # stores the link to the TF graph
    graph = None

    def __init__(self, schema: Schema):

        self.schema = schema

        self.graph = tf.get_default_graph()

        self.text = " ".join(schema.all_names)

        # We sample a new sequence every `step` characters
        step = 3

        # This holds our extracted sequences
        sentences = []

        # This holds the targets (the follow-up characters)
        next_chars = []

        for i in range(0, len(self.text) - self.maxlen, step):
            sentences.append(self.text[i: i + self.maxlen])
            next_chars.append(self.text[i + self.maxlen])
        print('Number of sequences:', len(sentences))

        # List of unique characters in the corpus
        self.chars = sorted(list(set(self.text)))
        print('Unique characters:', len(self.chars))
        # Dictionary mapping unique characters to their index in `chars`
        self.char_indices = dict((char, self.chars.index(char)) for char in self.chars)

        # Next, one-hot encode the characters into binary arrays.
        print('Vectorization...')
        self.x = np.zeros((len(sentences), self.maxlen, len(self.chars)), dtype=np.bool)
        self.y = np.zeros((len(sentences), len(self.chars)), dtype=np.bool)
        for i, sentence in enumerate(sentences):
            for t, char in enumerate(sentence):
                self.x[i, t, self.char_indices[char]] = 1
            self.y[i, self.char_indices[next_chars[i]]] = 1

        from keras import layers

        self.model = keras.models.Sequential()
        self.model.add(layers.LSTM(schema.neurons, input_shape=(self.maxlen, len(self.chars))))
        self.model.add(layers.Dense(len(self.chars), activation='softmax'))

        optimizer = keras.optimizers.RMSprop(lr=0.01)
        self.model.compile(loss='categorical_crossentropy', optimizer=optimizer)

    def fit_and_save(self):
        """ Train the model """
        self.model.fit(self.x, self.y,
                       batch_size=128,
                       epochs=self.schema.epochs)

        self.model.save(self.schema.modelname)

    def load_model(self):
        """ Loads a pretrained model. Returns False if this was unsuccesful """
        from keras.models import load_model

        self.graph = tf.get_default_graph()

        filepath = os.path.join('models', f'{self.schema.modelname}.h5')
        if os.path.isfile(filepath):
            self.model = load_model(filepath)
            return True
        else:
            return False

    def string_to_namelist(self, namestring):
        """ Converts a sting of names to a formatted list of names """
        names = set(namestring.split(" "))

        def process_title(n):
            n = n.replace('_', ' ')
            if self.schema.ucfirst:
                n = n[0].upper() + n[1:]
            if self.schema.fix_case:
                n = n.title()
            return n

        names_filtered = [process_title(name) for name in names if
                              name not in self.schema.all_names and len(name) > 2]
        return names_filtered

    def sample(self, predictions, temperature=1.0):
        """ """
        preds = np.asarray(predictions).astype('float64')
        preds = np.log(preds) / temperature
        exp_preds = np.exp(preds)
        preds = exp_preds / np.sum(exp_preds)
        probas = np.random.multinomial(1, preds, 1)
        return np.argmax(probas)

    def generate_names(self, number_of_names: int, temperature: float = 1.2):
        with self.graph.as_default():
            # Select a text seed at random
            start_index = random.randint(0, len(self.text) - self.maxlen - 1)
            generated_text = self.text[start_index: start_index + self.maxlen]
            print('--- Generating with seed: "' + generated_text + '"')
            print('------ temperature:', temperature)
            sys.stdout.write(generated_text)
            namestring = ""

            # We generate 400 characters
            for i in range(number_of_names):
                sampled = np.zeros((1, self.maxlen, len(self.chars)))
                for t, char in enumerate(generated_text):
                    sampled[0, t, self.char_indices[char]] = 1.

                preds = self.model.predict(sampled, verbose=0)[0]
                next_index = self.sample(preds, temperature)
                next_char = self.chars[next_index]

                generated_text += next_char
                namestring += next_char
                generated_text = generated_text[1:]
                # print(generated_text)
                # sys.stdout.write(next_char)
                # sys.stdout.flush()

            generated_names = self.string_to_namelist(namestring)
            print(generated_names)
            print(len(generated_text))
            print(len(generated_names))
            return generated_names
            # print()
