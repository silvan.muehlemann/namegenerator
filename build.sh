#!/bin/bash
cd frontend/namegenerator
yarn build
cd ../..
docker build -t registry.appuio.ch/silvan-privat/namegenerator .
oc project silvan-privat
docker push registry.appuio.ch/silvan-privat/namegenerator:latest
oc tag silvan-privat/namegenerator:latest mupo-shared/namegenerator:latest